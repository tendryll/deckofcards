$(document).ready(function () {
    // Returns the suit abbreviation
    function getSuitAbbr(suit) {
        return suit[0];
    }

    // Returns the rank abbreviation.
    function getRankAbbr(rank) {
        let abbr = "";

        switch (rank) {
            case "ACE":
            case "KING":
            case "QUEEN":
            case "JACK":
                abbr = rank[0];
                break;
            case "TEN":
                abbr = "10";
                break;
            case "NINE":
                abbr = "9";
                break;
            case "EIGHT":
                abbr = "8";
                break;
            case "SEVEN":
                abbr = "7";
                break;
            case "SIX":
                abbr = "6";
                break;
            case "FIVE":
                abbr = "5";
                break;
            case "FOUR":
                abbr = "4";
                break;
            case "THREE":
                abbr = "3";
                break;
            case "TWO":
                abbr = "2";
                break;
            default:
                break;
        }

        return abbr;
    }

    // Gets the next card from the deck and displays it.
    function loadCard() {
        $.get("api/dealcard", function (data) {
            if (data !== "") {
                let rank = getRankAbbr(data.rank);
                let suit = getSuitAbbr(data.suit);
                let image = `images/${rank}${suit}.png`;

                $("#card").attr("src", image);

                $("#notification").text("");
            } else {
                $("#notification").text("There are no more cards. Press the 'Shuffle' button.");
            }
        });
    }

    // Clicking on the Deal button will load the next card.
    $("#deal").click(function () {
        loadCard();
    });

    // Clicking on the Shuffle button will execute a request to shuffle the cards.
    $("#shuffle").click(function () {
        $.get("api/shuffle", function (isSuccessful) {
            if (isSuccessful) {
                $("#notification").text("Shuffling the cards was successful.");
            } else {
                $("#notification").text("Shuffling the cards was not successful.");
            }
        });
    });

    loadCard();
});