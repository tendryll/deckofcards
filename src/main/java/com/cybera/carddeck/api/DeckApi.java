package com.cybera.carddeck.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybera.carddeck.exception.OutOfCardsException;
import com.cybera.carddeck.model.Card;
import com.cybera.carddeck.service.DeckService;

/**
 * DeckApi defines two end points: /shuffle and /dealcard.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
@RestController
@RequestMapping(path = "/api", produces = "application/json")
@CrossOrigin(origins = "*")
public class DeckApi {
  private final DeckService deckService;

  /**
   * This constructor injects the DeckService into this class.
   *
   * @param deckService A reference to a {@link DeckService } implementation.
   */
  @Autowired
  public DeckApi(final DeckService deckService) {
    this.deckService = deckService;
  }

  /**
   * The shuffle method executes the shuffle method and returns true when successful, otherwise false.
   *
   * @return True when successful, otherwise false.
   */
  @GetMapping("/shuffle")
  public boolean shuffle() {
    return deckService.shuffle();
  }

  /**
   * The dealCard method executes the dealOneCard method and returns a single card if there are cards remaining,
   * otherwise a null value is returned.
   *
   * @return A single card when there are cards remaining, otherwise a null value is returned.
   */
  @GetMapping("/dealcard")
  public Card dealCard() {
    try {
      return deckService.dealOneCard();
    } catch (OutOfCardsException ex) {
      return null;
    }
  }
}
