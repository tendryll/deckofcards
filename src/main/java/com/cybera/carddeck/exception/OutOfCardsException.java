package com.cybera.carddeck.exception;

/**
 * Thrown to indicate that there are no more cards to select from the card deck.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public class OutOfCardsException extends RuntimeException {
  public OutOfCardsException(String message) {
    super(message);
  }
}
