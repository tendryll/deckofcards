package com.cybera.carddeck.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DeckWeb {

  @GetMapping(value = "/home")
  public String showDeckOfCards(final Model model) {
    return "index";
  }
}
