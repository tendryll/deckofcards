package com.cybera.carddeck.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybera.carddeck.exception.OutOfCardsException;
import com.cybera.carddeck.model.Card;
import com.cybera.carddeck.repository.DeckRepository;
import com.cybera.carddeck.service.DeckService;

/**
 * {@link DeckServiceImpl} is responsible for manipulating a deck of cards.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
@Service
public class DeckServiceImpl implements DeckService {
  private static int CARD_INDEX = 0;

  private final DeckRepository deckRepository;

  /**
   * Injects and sets the DeckRepository implementation.
   *
   * @param deckRepository An implementation of the {@link DeckRepository}.
   */
  @Autowired
  public DeckServiceImpl(final DeckRepository deckRepository) {
    this.deckRepository = deckRepository;
  }

  @Override
  public boolean shuffle() {
    final List<Card> deckOfCards = deckRepository.getDeck();
    final List<Card> previousDeckOfCards = new ArrayList<>(deckOfCards);

    // reset the card index
    CARD_INDEX = 0;

    for (int i = 0; i < deckOfCards.size(); i++) {
      final int randomIndex = (int) Math.floor(Math.random() * deckOfCards.size());

      final Card temp = deckOfCards.get(i);
      deckOfCards.set(i, deckOfCards.get(randomIndex));
      deckOfCards.set(randomIndex, temp);
    }

    return !previousDeckOfCards.equals(deckOfCards);
  }

  @Override
  public Card dealOneCard() throws OutOfCardsException {
    final List<Card> cards = deckRepository.getDeck();

    if (CARD_INDEX >= cards.size()) {
      throw new OutOfCardsException("Out of cards.");
    }

    final Card card = cards.get(CARD_INDEX);

    CARD_INDEX++;

    return card;
  }
}
