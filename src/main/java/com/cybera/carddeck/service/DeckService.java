package com.cybera.carddeck.service;

import java.util.List;

import com.cybera.carddeck.exception.OutOfCardsException;
import com.cybera.carddeck.model.Card;

/**
 * Manipulates a deck of cards.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public interface DeckService {
  /**
   * Shuffles a deck of cards.
   *
   * @return Returns true when the shuffle was successful, otherwise false.
   */
  boolean shuffle();

  /**
   * Returns a single card. Throws a {@link OutOfCardsException} when no more cards are eligible to be selected.
   *
   * @return A single @{@link Card}.
   * @throws OutOfCardsException Thrown when no more cards are available from the card deck.
   */
  Card dealOneCard() throws OutOfCardsException;
}
