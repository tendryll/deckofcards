package com.cybera.carddeck.model;

import java.util.Objects;
import java.util.StringJoiner;

import com.cybera.carddeck.model.enumerations.Rank;
import com.cybera.carddeck.model.enumerations.Suit;

/**
 * Represents a playing card.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public class Card {
  private Rank rank; // the card's rank (eg. Ace, King, Queen, Jack, 10, ... 2)
  private Suit suit; // the card's suit (Heart, Diamond, Club, Spade)

  public Card(Rank rank, Suit suit) {
    this.rank = rank;
    this.suit = suit;
  }

  /**
   * Returns the card's rank.
   *
   * @return The card's rank.
   */
  public Rank getRank() {
    return rank;
  }

  /**
   * Returns the card's suit.
   *
   * @return The card's suit.
   */
  public Suit getSuit() {
    return suit;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Card card = (Card) o;
    return rank == card.rank &&
        suit == card.suit;
  }

  @Override
  public int hashCode() {
    return Objects.hash(rank, suit);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Card.class.getSimpleName() + "[", "]")
        .add("rank=" + rank)
        .add("suit=" + suit)
        .toString();
  }
}
