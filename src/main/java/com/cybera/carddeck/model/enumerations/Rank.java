package com.cybera.carddeck.model.enumerations;

public enum Rank {
  ACE("ace"),
  KING("king"),
  QUEEN("queen"),
  JACK("jack"),
  TEN("ten"),
  NINE("nine"),
  EIGHT("eight"),
  SEVEN("seven"),
  SIX("six"),
  FIVE("five"),
  FOUR("four"),
  THREE("three"),
  TWO("two");

  private String rank;

  Rank(String rank) {
    this.rank = rank;
  }

  public String getRank() {
    return rank;
  }
}
