package com.cybera.carddeck.model.enumerations;

public enum Suit {
  HEARTS("hearts"),
  DIAMONDS("diamonds"),
  CLUBS("clubs"),
  SPADES("spades");

  private String suit;

  Suit(String suit) {
    this.suit = suit;
  }

  public String getSuit() {
    return suit;
  }
}
