package com.cybera.carddeck.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cybera.carddeck.model.Card;
import com.cybera.carddeck.model.enumerations.Rank;
import com.cybera.carddeck.model.enumerations.Suit;
import com.cybera.carddeck.repository.DeckRepository;

@Repository
public class DeckRepositoryImpl implements DeckRepository {
  private static List<Card> DECK_OF_CARDS;

  @Override()
  public List<Card> getDeck() {
    if (DECK_OF_CARDS == null) {
      DECK_OF_CARDS = new ArrayList<>();

      for (Suit suit : Suit.values()) {
        for (Rank rank : Rank.values()) {
          DECK_OF_CARDS.add(new Card(rank, suit));
        }
      }
    }

    return DECK_OF_CARDS;
  }
}
