package com.cybera.carddeck.repository;

import java.util.List;

import com.cybera.carddeck.model.Card;

/**
 * Represents the deck of card's data layer.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public interface DeckRepository {

  /**
   * Returns a full deck of cards.
   *
   * @return A deck of cards.
   */
  List<Card> getDeck();
}
