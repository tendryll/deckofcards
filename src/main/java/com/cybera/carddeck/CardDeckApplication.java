package com.cybera.carddeck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardDeckApplication {

  public static void main(String[] args) {
    SpringApplication.run(CardDeckApplication.class, args);
  }

}
