Feature: Shuffling cards

  Scenario: Clicking the Shuffle button returns a success message to the user
    Given the webpage of "http://localhost:8080/"
    When the shuffle button is clicked
    Then the notification message shows the message 'Shuffling the cards was successful.'