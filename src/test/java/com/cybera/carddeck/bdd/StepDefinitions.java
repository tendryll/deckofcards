package com.cybera.carddeck.bdd;

import static org.junit.Assert.assertEquals;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {
  private final WebDriver driver;

  public StepDefinitions() {
    // System.setProperty("webdriver.gecko.driver", "/home/bin");
    driver = new FirefoxDriver();
  }

  @After()
  public void post() {
    driver.quit();
  }

  @Given("the webpage of {string}")
  public void the_webpage_of(final String webAddress) {
    driver.get(webAddress);
  }

  @When("the shuffle button is clicked")
  public void the_shuffle_button_is_clicked() {
    driver.findElement(By.id("shuffle")).click();
  }

  @Then("the notification message shows the message {string}")
  public void the_notification_message_shows_the_message(final String msg) {
    new WebDriverWait(driver, Duration.ofSeconds(10).toSeconds())
        .until((ExpectedCondition<Boolean>) drive -> drive.findElement(By.id("notification")).getText().length() != 0);

    assertEquals(msg, driver.findElement(By.id("notification")).getText());
  }
}
