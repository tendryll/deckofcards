package com.cybera.carddeck.bdd;

import org.springframework.boot.test.context.SpringBootTest;

import com.cybera.carddeck.CardDeckApplication;

import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = CardDeckApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CucumberSpringConfiguration {
}