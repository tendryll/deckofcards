package com.cybera.carddeck.repository.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.cybera.carddeck.model.Card;
import com.cybera.carddeck.model.enumerations.Rank;
import com.cybera.carddeck.model.enumerations.Suit;
import com.cybera.carddeck.repository.DeckRepository;

class DeckRepositoryImplTest {
  private DeckRepository deckRepository;

  @BeforeEach
  void setUp() {
    deckRepository = new DeckRepositoryImpl();
  }

  @Test
  void getDeck() {
    final List<Card> actual = deckRepository.getDeck();

    for (Suit suit : Suit.values()) {
      for (Rank rank : Rank.values()) {
        assertTrue(actual.contains(new Card(rank, suit)));
      }
    }
  }
}