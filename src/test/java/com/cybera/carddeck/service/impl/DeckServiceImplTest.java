package com.cybera.carddeck.service.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.cybera.carddeck.exception.OutOfCardsException;
import com.cybera.carddeck.model.Card;
import com.cybera.carddeck.repository.DeckRepository;
import com.cybera.carddeck.repository.impl.DeckRepositoryImpl;
import com.cybera.carddeck.service.DeckService;

class DeckServiceImplTest {
  private final DeckRepository deckRepository = new DeckRepositoryImpl();
  private DeckService deckService;

  @BeforeEach
  void setUp() {
    this.deckService = new DeckServiceImpl(deckRepository);
  }

  @Test
  void shuffle() {
    // clone the deck
    final List<Card> deckOfCards = new ArrayList<>(deckRepository.getDeck());

    // shuffle the deck
    final boolean actual = deckService.shuffle();

    // confirm that the deck was shuffled
    assertEquals(!deckOfCards.equals(deckRepository.getDeck()), actual);
  }

  @Test
  void dealOneCard() {
    final Card card = deckService.dealOneCard();

    // verify that the card returned is not null
    assertNotNull(card);

    // execute the dealOneCard method 51 more times to deal the maximum number of cards.
    for (int i = 0; i < 51; i++) {
      deckService.dealOneCard();
    }

    // verify that an OutOfCardsException is thrown
    final OutOfCardsException ex =
        assertThrows(OutOfCardsException.class, () -> deckService.dealOneCard());
    assertEquals("Out of cards.", ex.getMessage());
  }
}