# Deck of Cards web application

This project uses Spring Boot to implement functionality to shuffle a deck of cards, and to take a single card from the deck. 
Java packages have been created to contain Java code for the web, api, service and repository layers. Additional packages 
have been created to contain code to represent a card and an exception.

This project contains unit and integration tests. The unit tests test the repository and service classes using JUnit 5. 
The integration test tests the shuffle web functionality. The integration test uses the Cucumber testing framework and Selenium Webdriver.